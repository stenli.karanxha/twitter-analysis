################################################################
#  The module contains different evaluation algorithms, each assigning a numerical value to a 
#  list TweetReduced object, and saving the result in a file for further elaboration / visualization.
#   - The TweetWordEval class: evaluates the text of the tweet based on a lookup table
#   - The TweetHashtagEval class: evaluates the presence of a list of hashtags in the tweet.
################################################################

from tweet import TRKeys,  TweetReduced,  TEKeys,  TweetEvaluated
import os.path

################################################################
# TweetWordEval class
################################################################
class TweetWordEval(object):
    """
        Implements the assignment of a sentiment to a tweet, based simply on the words 
        contained in the text part of the tweet.
    """
    
    def __init__(self, output_file_name,  lookup_file_name):
        """
        Constructor
        """
        self._output_file_name = output_file_name
        self._init_lookup_table(lookup_file_name)

    def run(self,  input_file_name):
        """
        Run the algorithm: reads one line at a time from a file containing reduced tweets, 
        and writes the evaluated tweet in the output file.
        """
        if not os.path.exists(input_file_name):
            return False
            
        with open(input_file_name, 'r') as input_file:
            with open(self._output_file_name, 'w') as output_file:
                for line in input_file:
                    tweet_reduced = TweetReduced()
                    tweet_reduced.deserialize(line)
                    if tweet_reduced.is_valid():
                        tweet_evalutated = self._calc_tweet_evaluated(tweet_reduced)
                        if tweet_evalutated.is_valid():
                            my_tweet_string = tweet_evalutated.serialize() + '\n'
                            output_file.write(my_tweet_string)
        return True

    def _init_lookup_table(self,  lookup_file_name):
        """
        Initializes an internal lookup table.
        """
        self._lookup_table = {}
        if not os.path.exists(lookup_file_name):
            return False
        
        with open(lookup_file_name, 'r') as file:
            for line in file:
                words = line.split()
                if len(words) == 2:
                    key = str(words[0]).lower()
                    value = int(words[1])
                    self._lookup_table[key] = value
        return True


    def _calc_tweet_evaluated(self, tweet):
        """
        Generates an evalutated tweet out of a reduced one. 
        """
        eval = self._eval_tweet_text(tweet.get_value(TRKeys.KEY_TEXT))
        geo = tweet.get_value(TRKeys.KEY_GEO)
        time = tweet.get_value(TRKeys.KEY_TIME)
        
        out = TweetEvaluated()
        out.set_value(TEKeys.KEY_EVAL,  eval)
        out.set_value(TRKeys.KEY_GEO,  geo)
        out.set_value(TRKeys.KEY_TIME,  time)
        return out


    def _eval_tweet_text(self,  text):
        """
        Calculates the value of a text, based on the lookup table.
        """
        out = 0.0
        words=text.split()
        for word in words:
            key=word.lower()
            if key in self._lookup_table:
                out+=self._lookup_table[key]
        return out
        


################################################################
# TweetHashtagEval class
################################################################
class TweetHashtagEval(object):
    
    def __init__(self,  output_file_name,  hashtags):
        """
        Constructor
        """
        self._output_file_name = output_file_name
        self._hashtags  = hashtags


    def run(self,  input_file_name):
        """
        Run the algorithm: reads one line at a time from a file containing reduced tweets, 
        and writes the evaluated tweet in the output file.
        """
        if not os.path.exists(input_file_name):
            return False
        
        with open(input_file_name, 'r') as input_file:
            with open(self._output_file_name, 'w') as output_file:
                for line in input_file:
                    tweet_reduced = TweetReduced()
                    tweet_reduced.deserialize(line)
                    if tweet_reduced.is_valid():
                        tweet_evalutated = self._calc_tweet_evaluated(tweet_reduced)
                        if tweet_evalutated.is_valid():
                            my_tweet_string = tweet_evalutated.serialize() + '\n'
                            output_file.write(my_tweet_string)
        return True

    def _calc_tweet_evaluated(self, tweet):
        """
        Generates an evalutated tweet out of a reduced one. 
        """
        eval = self._eval_tweet_text(tweet.get_value(TRKeys.KEY_HASHTAG))
        geo = tweet.get_value(TRKeys.KEY_GEO)
        time = tweet.get_value(TRKeys.KEY_TIME)
        
        out = TweetEvaluated()
        out.set_value(TEKeys.KEY_EVAL,  eval)
        out.set_value(TRKeys.KEY_GEO,  geo)
        out.set_value(TRKeys.KEY_TIME,  time)
        return out

    def _eval_tweet_text(self,  tweet_hashtags):
        """
        Calculates the value of a text, based on the presence of one or more
        of the hashtags.
        """
        out = 0.0
        for hashtag in self._hashtags:
            if hashtag in tweet_hashtags:
                out += 1.0
        return out

