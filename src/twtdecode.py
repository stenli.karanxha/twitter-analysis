################################################################
#  Transforms a string containing a full tweet into a simpler TweetReduced object.
#  Contains:
#   - TweetDecode, class providing the decoding algorithm.
#   - Utility functions to implement the single steps of the algorithm.
################################################################

import json
from tweet import TweetReduced,  TRKeys

################################################################
# TweetDecode class 
################################################################
class TweetDecode(object):
    """
        Contains a decoding algorithm, to transform a full tweet into a TweetReduced object.
    """

    def __init__(self,  tweet_line):
        self._tweet_line = tweet_line

    def get_reduced_tweet(self):
        full_tweet = json.loads(self._tweet_line)

        out = TweetReduced()
        out.set_value(TRKeys.KEY_TEXT, _get_text(full_tweet))
        out.set_value(TRKeys.KEY_HASHTAG,  _get_hashtags(full_tweet))
        out.set_value(TRKeys.KEY_GEO,  _get_geo(full_tweet))
        out.set_value(TRKeys.KEY_TIME,  _get_time(full_tweet))
        out.set_value(TRKeys.KEY_LANG,  _get_language(full_tweet))
        
        return out


################################################################
# Utility functions for the single steps of the decoding
################################################################

KEY_TEXT = "text"

def _get_text(full_tweet):
    """
        Returns the text if available in the tweet, otherwise returns None.
    """
    if KEY_TEXT in full_tweet:
        return full_tweet[KEY_TEXT]
    else:
        return None


KEY_ENTITIES = 'entities'
KEY_HASHTAGS = 'hashtags'
KEY_HASHTAGS_TEXT = 'text'

def _get_hashtags(full_tweet):
    """
        Returns the hashtags of the tweet.
        We also accept tweets with no hashtags, and in this case an empty list
        instead of None is returned.
    """
    out = []
    if _has_hashtags(full_tweet):
        for hashtag in full_tweet[KEY_ENTITIES][KEY_HASHTAGS]:
            if KEY_HASHTAGS_TEXT in hashtag and hashtag[KEY_HASHTAGS_TEXT]:
                out.append(hashtag[KEY_HASHTAGS_TEXT])
    return out

def _has_hashtags(full_tweet):
    return KEY_ENTITIES in full_tweet and \
        full_tweet[KEY_ENTITIES]  and \
        KEY_HASHTAGS in full_tweet[KEY_ENTITIES] and \
        full_tweet[KEY_ENTITIES][KEY_HASHTAGS] 


KEY_COORDINATES = 'coordinates'
KEY_PLACE = 'place'
KEY_BOUNDING_BOX = 'bounding_box'

def _get_geo(full_tweet):
    """
        Returns the coordinates of the tweet. 
        If available, the coordinates of the generation point are used. 
        Otherwise the coordinates of the place associated with the tweet are used.
        If also those are missing, then the function returns None.
    """
    if _has_generated_coordinates(full_tweet):
        return full_tweet[KEY_COORDINATES][KEY_COORDINATES]
    elif _has_place_coordinates(full_tweet):
        return _get_polygon_center(full_tweet[KEY_PLACE][KEY_BOUNDING_BOX] [KEY_COORDINATES][0])
    else:
        return None


def _has_generated_coordinates(full_tweet):
    return KEY_COORDINATES in full_tweet and full_tweet[KEY_COORDINATES] 


def _has_place_coordinates(full_tweet):
    return KEY_PLACE in full_tweet  and \
        full_tweet[KEY_PLACE]  and \
        KEY_BOUNDING_BOX in full_tweet[KEY_PLACE] and \
        full_tweet[KEY_PLACE][KEY_BOUNDING_BOX]  and \
        KEY_COORDINATES in full_tweet[KEY_PLACE][KEY_BOUNDING_BOX] 

def _get_polygon_center(points):
    if points == None:
        return None
    center = [0.0,  0.0]
    count = 0.0
    for point in points:
        center[0] += point[0]
        center[1] += point[1]
        count += 1
    if count > 0:
        center[0] /= count
        center[1] /= count
    return center


KEY_TIME = 'created_at'

def _get_time(full_tweet):
    """
        Returns the creation time if available in the tweet, otherwise returns None.
    """
    if KEY_TIME  in full_tweet:
        return full_tweet[KEY_TIME]
    else:
        return None
    
KEY_LANG = "lang"
VALUE_ENGLISH = 'en'
    
def _get_language(full_tweet):
    """
        As a test, only selects the tweets in english.
    """
    if KEY_LANG  in full_tweet and full_tweet[KEY_LANG] == VALUE_ENGLISH:
        return full_tweet[KEY_LANG]
    else:
        return None
