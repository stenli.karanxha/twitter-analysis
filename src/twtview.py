################################################################
#  The module represents the tweets in a map, using their geographic coordinates.
#  Contains: 
#   - TweetView class, public interface preparing a list of tweets from a file.
#   - Utility functions, to complete the task of drawing the tweets.
################################################################

loaded_correctly = False
try:
    from mpl_toolkits.basemap import Basemap
    import matplotlib.pyplot as plt
    loaded_correctly = True
except:
    print 'Please install mpl_toolkits.basemap from http://matplotlib.org/basemap/users/installing.html'

import numpy as np
import os.path
import time

from tweet import TEKeys,  TweetEvaluated, get_epoch_based_time

################################################################
# TweetView class
################################################################
class TweetView(object):
    """
        Implements the assignment of a sentiment to a tweet, based simply on the words 
        contained in the text part of the tweet.
    """
    
    def __init__(self, input_file_name,  color_scheme):
        """
        Constructor
        """
        self._read_tweets(input_file_name)
        self._color_scheme = color_scheme

    def view_all(self):
        """
            Base visualization function. 
            Shows all the tweets of the input file in a single scatter-plot.
        """
        if not loaded_correctly:
            return False
        val_range=_get_tweet_range(self._tweet_list)	
        _setmap()
        _plot_tweet_points(self._tweet_list, val_range, self._color_scheme)
        plt.show()

    def view_timed(self,  time_group):
        """
            Advanced visualization function. Groups and shows together only 
            tweets inside a time of time_group (in seconds), and presents 
            one picture every second. 
        """
        if not loaded_correctly:
            return False
        _setmap()
	plt.ion()
	plt.show()
        partial_list = []
        old_time = 0
        for tweet in self._tweet_list:
            act_time = tweet[3]
            if act_time - old_time > time_group:
                old_time = act_time
                if len(partial_list) > 0:
                    val_range=_get_tweet_range(partial_list)
                    _plot_tweet_points(partial_list, val_range, self._color_scheme)
                    plt.draw()
                    partial_list = []
                    time.sleep(1.0)
            else:
                partial_list.append(tweet)

        
    def _read_tweets(self,  input_file_name):
        self._tweet_list = []
        if not os.path.exists(input_file_name):
            return
        with open(input_file_name, 'r') as input_file:
            for ID, line in enumerate(input_file):
                tweet_evalutated = TweetEvaluated()
                tweet_evalutated.deserialize(line)
                if tweet_evalutated.is_valid():
                    value = tweet_evalutated.get_value(TEKeys.KEY_EVAL)
                    geo = tweet_evalutated.get_value(TEKeys.KEY_GEO)
                    utc_time = tweet_evalutated.get_value(TEKeys.KEY_TIME)
                    time = get_epoch_based_time(utc_time)
                    tweet_view = (ID, value, geo, time)
                    self._tweet_list.append(tweet_view)

################################################################
#  Utility functions
################################################################
def _setmap(map_kind='cyl',leftlat=-90.0,rightlat=90.0,leftlon=-180.0,rightlon=189.0,resol='c',area=0.1, meridians = 'off'):
    """ Sets up a map for a specified part of the world.
    Default values show the whole world in an equidistant cylindrical projection including all areas 
    larger than 0.1 sqkm."""
    global map
    # initialise the map using Basemap module	
    map = Basemap(projection=map_kind, resolution = resol, area_thresh=area, llcrnrlon = leftlon, \
        llcrnrlat = leftlat, urcrnrlon = rightlon, urcrnrlat = rightlat)
    # fill map 
    map.drawcoastlines()
    map.drawcountries()
    map.fillcontinents()
    #map.bluemarble()	
    map.drawmapboundary()
    
    # draw meridians
    if meridians == 'on':
        map.drawmeridians(np.arange(0, 360, 30))
        map.drawparallels(np.arange(-90, 90, 30))	

def _points_plot(colors, coordinates, color_scheme, size=20):
    """Makes a scatterplot of geographical data points on a 
        map according to a given colorscheme.
    Takes the colorvalues and coordinates of the data points, as 
    well as colorscheme and size of the points as input and plots 
    using the required colorscheme"""

    lon = [coord[0] for coord in coordinates]
    lat = [coord[1] for coord in coordinates]
    x,y = map(lon,lat)
    if color_scheme == 'binary':
        map.scatter(x,y,c=colors,s=size,zorder=4)
    else: 
        map.scatter(x,y,c=colors,cmap=color_scheme,s=size,zorder=4)

def _values_to_colors(tweet_values, valrange, color_scheme='binary'):
    """Transforms values into a certain colors.

        Takes as input a list of values, the range of the values as tuple and a color scheme 
        which is set by default to binary. 
        Transforms the values either to red for positive and blue for negative normvalues 
        (binary color scheme) or to floats between zero and one. Returns these values as a list.
    """
    color_list = []
    if color_scheme == 'binary':
        middle = (valrange[0]+valrange[1])/2
        for value in tweet_values:
            if value < middle:
                color_list.append('b')
            if value >= middle:
                color_list.append('r')
    else:
        for value in tweet_values:
            den = (valrange[1]-valrange[0])
            if den > 0:
                normval = (value-valrange[0]) / den
            else:
                normval = 0.0
            color_list.append(normval)
    return color_list


def _plot_tweet_points(tweet_list, valrange, color_scheme):
    """Receives a list of tweets and transforms their values to a scatterplot on the map.

    Takes as input a list containing tweets structured as a tuple (ID,value,coordinates(longitude,lattitude)), 
    the range of the values associated with the tweets and a color scheme. 

    Uses the function values_to_colors to transform the values to colorvalues 
    and points_plot to make a scatterplot of these values according to the color scheme."""

    value_list = [tweet[1] for tweet in tweet_list]	
    colors = _values_to_colors(value_list, valrange, color_scheme)
    coordinates = [tweet[2] for tweet in tweet_list]	
    _points_plot(colors, coordinates, color_scheme) 		


def _get_tweet_range(tweet_list):
    """Receives a list of tweets associated with a value and computes the value-range.
    For each tweet the value is in the second position of the tuple. 
    The function returns a tuple giving the minimum and the maximum value"""

    max = min = 0	
    for tweet in tweet_list:
        if tweet[1] < min:
            min = tweet[1]
        if tweet[1] > max:
            max = tweet[1]
    return(float(min),float(max))
