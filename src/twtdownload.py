################################################################
#  The module executes the following functionalities:
#   - Establishes a connection to the twitter server API.
#   - Downloads tweets corresponding to certain criteria.
#   - Decodes them into Tweet objects.
#   - Saves the Tweets in a file for further elaborations.
#  It contains:
#   - The TweetDownload class which runs the algorithm
#   - Utility functions to create an authenticated connection to the twitter server.
#  Credit:
#   - The _get_twitter_response function and the constants it 
#     is using have been copied / adapted from the ProjectB, 
#     TwitterFiltering.
################################################################

import oauth2 as oauth
import urllib2 as urllib
import json
import datetime as dt
import time

from twtdecode import TweetDecode

################################################################
# TweetDownload class
################################################################
class TDRunTypes(object):
    rt_time = 1
    rt_count = 2

class TweetDownload(object):
    """
        Contains the downloading / filtering / saving algorithm
    """
    def __init__(self,  keywords,  output_file_name):
        """
        Inits the module data: 
            - the twitter server url, prepared with the keywords asked from the user.
            - the twitter response, iteratable connection to the server containing the tweets.
            - output file name, where the decoded tweets will be written.
        """
        self._twitter_url = "https://stream.twitter.com/1/statuses/sample.json"
        self._twitter_response = _get_twitter_response(self._twitter_url)
        self._output_file_name = output_file_name


    def run(self, run_type,  run_parm):
        """
        Runs the module.
            - Checks that a proper initialization has taken place.
            - Download and writes tweets, using either a limit of time or of number of tweets.
        """
        if not self._twitter_url or not self._twitter_response or not self._output_file_name:
            return False
        if run_type == TDRunTypes.rt_time:
            self._fetch_tweets_timed(run_parm)
        else:
            self._fetch_tweets_counted(run_parm)
        return True


    def _fetch_tweets_counted(self,  max_num_tweets):
        """From the twitter response (stream object) gets max_num_tweets
            that can be decoded into Tweet objects. 
        """
        count = 0
        for line in self._twitter_response:
            if self._write_decoded_tweet(line):
                count += 1
                print count
            if count >= max_num_tweets:
                break


    def _fetch_tweets_timed(self,  max_time):
        """From the twitter response (stream object) gets tweets until max_time 
            has passed. 
            max_time: time in seconds.
        """
        start_time = _get_time()
        for line in self._twitter_response:
            self._write_decoded_tweet(line)
            actual_time = _get_time()
            if actual_time >= start_time + max_time:
                break


    def _write_decoded_tweet(self,  tweet_line):
        """
            Tries to decode a line of text representing a tweet into a Tweet object.
            If successful, appends the object at the end of the output file.
        """
        my_decoder = TweetDecode(tweet_line)
        my_reduced_tweet = my_decoder.get_reduced_tweet()
        if my_reduced_tweet.is_valid():
            with open(self._output_file_name, 'a') as output_file:
                my_tweet_string = my_reduced_tweet.serialize() + '\n'
                output_file.write(my_tweet_string)
            return True
        else:
            return False


################################################################
# Utility functions for the single steps of the downloading
################################################################
def _get_time():
    """
    Returns the time in seconds since the epoch
    """
    return time.mktime(dt.datetime.now().timetuple())


"""
Connection to the twitter server: hardcoded definitions for the authentication.
"""
_access_token_key = "1960885710-AhVlOKoBlYLxYDUFbhXA1DUF1rLYjnOlH8LH9a8"
_access_token_secret = "cDlvhJAFjXx0Xr7Sjqi6YecmJdPwRmmGvPDKeTo7IE"
_oauth_token    = oauth.Token(key=_access_token_key, secret=_access_token_secret)

_consumer_key = "kHsWFVEOHBZEV9DY8eqg"
_consumer_secret = "YrXdGvGjdYJChzfnd7cJqdZLT74F9cKLQvorltPTuY"
_oauth_consumer = oauth.Consumer(key=_consumer_key, secret=_consumer_secret)

_signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

_debug = 0
_http_handler  = urllib.HTTPHandler(debuglevel=_debug)
_https_handler = urllib.HTTPSHandler(debuglevel=_debug)

_http_method = "GET"

def _get_twitter_response(twitter_url):
    """Construct, sign, and open a twitter request using the hard coded credentials. 
    """
    req = oauth.Request.from_consumer_and_token(_oauth_consumer,
                                             token=_oauth_token,
                                             http_method=_http_method,
                                             http_url=twitter_url, 
                                             parameters=[])
    req.sign_request(_signature_method_hmac_sha1, _oauth_consumer, _oauth_token)

    opener = urllib.OpenerDirector()
    opener.add_handler(_http_handler)
    opener.add_handler(_https_handler)
    response = opener.open(req.to_url(), None)
    return response
