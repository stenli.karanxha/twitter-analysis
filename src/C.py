################################################################
#  Common definitions shared by all modules
################################################################
download_output_file_name = './download_tweets.temp'
download_time = 60*5    # Time is expressed in seconds
download_count = 10	# Number of tweets to download

word_lookup_file_name = './AFINN-111.txt'
word_eval_output_file_name = './eval_sent_tweets.temp'

hashtags_to_seek = ["TTP", "Pakistan"]
hashtag_eval_output_file_name = './eval_hashtag_tweets.temp'


################################################################
#  Tweet downloading and pre-filtering.
################################################################
from twtdownload import TweetDownload,  TDRunTypes

def execute_download():
    print 'Start download.'
    myDownloadAlgorithm = TweetDownload('',  download_output_file_name)
    myDownloadAlgorithm.run(TDRunTypes.rt_count,  download_count)
    #myDownloadAlgorithm.run(TDRunTypes.rt_time,  download_time)
    print 'End download.'
    

################################################################
#  Tweet numerical evaluation.
################################################################
from twteval import TweetWordEval,  TweetHashtagEval

def execute_eval():
    print 'Start evaluation.'
    mySentimentEvalAlgorithm = TweetWordEval(word_eval_output_file_name,  word_lookup_file_name)
    mySentimentEvalAlgorithm.run(download_output_file_name)
    
    myHashtagEvalAlgorithm = TweetHashtagEval(hashtag_eval_output_file_name,  hashtags_to_seek)
    myHashtagEvalAlgorithm.run(download_output_file_name)
    print 'End evaluation.'
    
    
################################################################
#  Tweet visualization.
################################################################
from twtview import TweetView

def execute_view():
    print 'Start viewing.'
    myViewingGenerator = TweetView(word_eval_output_file_name,  'seismic')
    myViewingGenerator.view_all()
    myViewingGenerator.view_timed(100)
    print 'End viewing.'

################################################################
#  Run
################################################################
if __name__ == '__main__':
    """
        Runs all three components: downloads, evaluates and views.
    """
    execute_download()
    execute_eval()
    execute_view()
