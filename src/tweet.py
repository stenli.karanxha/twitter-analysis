################################################################
#  Serializable classes for managing the tweet data.
#   - TweetBase: provides the serialization and invariant checking for the next classes.
#   - TweetReduced: contains a reduced, amounts of fields from the full tweet. 
#   - TweetEvaluated: contain only the space-time coordinates and a value for each tweet.
################################################################

import json
import calendar
import time

################################################################
#  TweetBase class
################################################################
class TweetBase(object):
    """
        Provides serialization and simple access to the data.
    """
    def serialize(self):
        return json.dumps(self._data)
        
    def deserialize(self,  input_string):
        try:
            self._data =json.loads(input_string)
        except Exception:
            pass
        
    def get_value(self,  key):
        if key in self._data:
            return self._data[key]
        return None
       
    def set_value(self,  key,  value):
        if key in self._data:
            self._data[key] = value


################################################################
#  TweetReduced class
################################################################
class TRKeys(object):
    """
        Contains the dictionary keys for the TweetReduced class.
    """
    KEY_TEXT = 'text'                      # text of the tweet
    KEY_HASHTAG = 'hashtag'         # hashtags of the tweet
    KEY_GEO = 'geo'                         # geographic coordinates
    KEY_TIME = 'time'                      # creation time of the tweet
    KEY_LANG = 'language'              # language of the tweet
    ALL_KEYS = {KEY_TEXT, KEY_HASHTAG,  KEY_GEO,  KEY_TIME, KEY_LANG}
    

class TweetReduced(TweetBase):
    """
        Contains a  reduced amount of information from the raw tweet.
        Ensures the invariant that the internal dict contains all the keys, and has no None fields.
    """
  
    def __init__(self):
        self._data = {TRKeys.KEY_TEXT : None,  TRKeys.KEY_HASHTAG : None, TRKeys.KEY_GEO : None,  TRKeys.KEY_TIME: None, TRKeys.KEY_LANG : None}

    def is_valid(self):
        return _check__dict_keys(TRKeys.ALL_KEYS,  self._data) and _check__dict_values(self._data)


################################################################
#  TweetEvaluated class
################################################################
class TEKeys(object):
    """
        Contains the dictionary keys for the TweetEvaluated class.
    """
    KEY_GEO = 'geo'                         # geographic coordinates
    KEY_TIME = 'time'                      # creation time of the tweet
    KEY_EVAL = 'eval'                      # numerical evaluation of the tweet
    ALL_KEYS = { KEY_GEO,  KEY_TIME, KEY_EVAL}
    

class TweetEvaluated(TweetBase):
    """
        Contains a  a value and space-time coordinates of the tweet.
        Ensures the invariant that the internal dict contains all the keys, and has no None fields.
    """
  
    def __init__(self):
        self._data = {TEKeys.KEY_GEO : None,  TEKeys.KEY_TIME: None, TEKeys.KEY_EVAL : None}

    def is_valid(self):
        return _check__dict_keys(TEKeys.ALL_KEYS,  self._data) and _check__dict_values(self._data)


################################################################
#  Utility functions
################################################################
def _check__dict_keys(keys,  input_dict):
    for key in keys:
        if not key in input_dict:
            return False
    return True

def _check__dict_values(input_dict):
    for key in input_dict:
        val = input_dict[key]
        if val == None:
            return False
    return True

def get_epoch_based_time(time_string):
    if time_string:
        time_clean_string = time_string.replace('+0000',  '')
        time_struct = time.strptime(time_clean_string)
        return calendar.timegm(time_struct)
    return None
